#Code to run the motion detection program

import motiondetect

t = motiondetect.MotionDetect()

print 'Webcam Motion Detection Program'

while (True):

    command = raw_input('Please enter a command, or type "help" for more information \n >: ')

    if command == 'help':
        print 'Commands: \n detect   --   Set the program to detect motion, for a certain amount of time, \n               without viewing the video feed. A photo will be taken when motion\n               is detected.\n view detect   --   Show the video feed and turn motion detection on, if motion \n                    is detected, photo will be saved. \n view   --   view the video feed with motion detection off, press the spacebar \n             while running to save a photo. \n quit   --   End the program \n'


    if command == 'detect':
        dur = input("Please enter time in seconds: ")
        t.detect(dur)

    if command == 'view detect':
        t.view_detect()

    if command == 'view':
        t.view()

    if command == 'quit':
        print('Quitting program...')
        break

    else: 
        print('Command not recognised, please try again.')
